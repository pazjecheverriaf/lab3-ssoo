/*para leer un archivo 
*/
#include<fstream>
#include<iostream>
#include <pthread.h>
#include <unistd.h>
#include <sstream>
#include <fcntl.h>
#include <string.h>
#include <time.h>

using namespace std;
//typedef struct 

void procesar_archivo (string archivo){
    ifstream file(archivo);
    string str;
    int cnt_lineas = 0;
    int cnt_car = 0;

    while (std::getline(file, str)){
        //contar linea 
        while (file.good()) if(file.get() == '\n') cnt_lineas++;
        cnt_lineas++;

        //contar caracteres de la linea
        for (int i=0; i<str.length(); i++) {
            cout << str[i];
            cnt_car++;
        }
        cout << endl;
    }

    cout << cnt_lineas << " lineas." << endl;
    cout << cnt_car << " caracteres." << endl;
}

void cnt_palabras(string archivo){
    char cadena[10];
    int palabras =0;
    int total_palabras =0;

    //se abre el archivo
    ifstream file(archivo);
    //se recorre el texto hasta el final
    while(!file.eof()){
        file >> cadena;
        palabras++;
    }

    file.close();

    cout << palabras <<" palabras." << endl;

    total_palabras += palabras;

}

int main(int argc, char *argv[]){
    float Ti, Tf;
    int total_palabras =0;
    int cnt_lineas =0;
    int cnt_car =0;

    for (int i=1; i<=argc ; i++){
        
        // se recorren los datos 
        cout << "------Archivo------" << argv[i] << "---" << endl;
        // se llaman a las funciones  
        Ti = clock();     
        procesar_archivo(argv[i]);
        cnt_palabras(argv[i]);
        int total_palabras;
        Tf = clock();
        cout << "el tiempo empleado es. " << (Tf-Ti)/CLOCKS_PER_SEC << "seg" << endl;
    }
    // se imprime un resun de los datos 

    cout << "total de lineas en los archivos. " << cnt_lineas << endl;
    cout << "total de caracteres en los archivos. " << cnt_car << endl;
    cout << "total de palabras en los archivos. " << total_palabras << endl;

    
    



    return 0;
}
#include <fstream>
#include <pthread.h>
#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <sstream>
#include <fcntl.h>
#include <string.h>
#include <time.h>

int total_palabras = 0;
int cnt_lineas = 0;
int cnt_car = 0;
using namespace std;

void * procesar_archivo(void *param){

    char *archivo;
    archivo = (char *) param;
    // se abre el archivo
    ifstream file(archivo);
    string str;
    int cnt_lineas = 0;
    int cnt_car = 0;

    while (std::getline(file, str))
    {
        // contar linea
        while (file.good()) if (file.get() == '\n')
        cnt_lineas++;
        
        cout << endl;
   

       // contar caracteres de la linea
        for (int i = 0; i < str.length(); i++)
        {
            cout << str[i];
            cnt_car++;
        }
        cout << endl;
        //pthread_exit(0);

   }

    cout << "total de lineas..." << cnt_lineas  << endl;
    cout << "total de caracteres..." << cnt_car  << endl;
    pthread_exit(0);
}

void *cnt_palabras(void *param)
{
    char cadena[1];
    char *archivo;
    archivo = (char *) param;
    int palabras = 0;
    int total_palabras = 0;

    // se abre el archivo
    ifstream file(archivo);
    // se recorre el texto hasta el final
    while (!file.eof())
    {
        file >> cadena;
        palabras++;
    }

    file.close();

    cout << "Total de palabras... " << palabras << endl;

    total_palabras += palabras;
    pthread_exit(0);
}

int main(int argc, char *archivos[])
{
    int total_palabras = 0;
    int cnt_lineas = 0;
    int cnt_car = 0;
    float Ti_1, Ti_2; 
    float Tf_1, Tf_2;
    pthread_t threads[argc -1];
    
    //se crean los hilos
    for (int i=0; i <argc -1; i++){
        cout << "-------------------Archivo------------------- "<< archivos[i+1]<< "..." << endl;
        sleep(0.001);

        Ti_1 = clock();
        pthread_create(&threads[i], NULL, procesar_archivo, archivos[i+1]);
        Tf_1 = clock();

        sleep(0.001);
         Ti_2 = clock();
        pthread_create(&threads[i], NULL, cnt_palabras, archivos[i+1]);
        Tf_2 = clock();
        sleep(0.001);
   
    }

    for (int i=0; i<argc -1;i++){
        pthread_join(threads[i], NULL);
    }
 
    // se impre un resun de los datos
    //cout << "total de lineas en los archivos. " << cnt_lineas << endl;
    //cout << "total de caracteres en los archivos. " << cnt_car << endl;
    //cout << "total de palabras en los archivos. " << total_palabras << endl;
    
    // Tiempo inicial
    float Tiempo_inicial = Ti_1 + Ti_2;
    //Tiempo final
    float Tiempo_final = Tf_1 + Tf_2;
    cout << "-------------------------" << endl;
    cout << "el tiempo empleado es. " << (Tiempo_final - Tiempo_inicial) / CLOCKS_PER_SEC << "seg" << endl;

    //return 0;
}